#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module for looking up stuff

require "yaml"
require "csv"
require_relative "adb" unless $cli

module Find
# commenting out next line because it's not needed for now
# extend Log  # make log() method available as shortcut for Log.add()
class << self

  @@codenames = nil
  @@recovery_partition_names = nil
  @@recovery_key_combinations = nil
  @@supported = nil
  @@brands = nil
  @@aliases = nil
  @@csv_file = "device-lookup.csv"
  @@table = nil
  @@supported_brands = ["samsung", "motorola", "oneplus", "nvidia", "sony"]

  def get_codenames
    Get.retry_on_http_connection_problem do
      @@codenames = YAML.load(URI.open("https://gitlab.com/free-droid/free-droid/raw/master/lookups/codenames.yml").read)
    end unless @@codenames
  end

  def get_csv
    Get.retry_on_http_connection_problem do
      Get.download "http://storage.googleapis.com/play_public/supported_devices.csv", @@csv_file
    end unless File.exist? @@csv_file
    return nil unless File.exist? @@csv_file
    @@table = CSV.read(@@csv_file, headers: true, encoding: "UTF-16:UTF-8") unless @@table
    @@table.is_a?(CSV::Table)
  end

  def get_brands
    Get.retry_on_http_connection_problem do
      @@brands = YAML.load(URI.open("https://gitlab.com/free-droid/free-droid/raw/master/lookups/brands.yml").read)
    end
  end

  def get_supported
    Get.retry_on_http_connection_problem do
      @@supported = YAML.load(URI.open("https://gitlab.com/free-droid/free-droid/raw/master/lookups/supported.yml").read)
    end unless @@supported
  end

  def get_recovery_partitions_names
    Get.retry_on_http_connection_problem do
      @@recovery_partition_names = YAML.load(URI.open("https://gitlab.com/free-droid/free-droid/raw/master/lookups/recovery_partition_names.yml").read)
    end unless @@recovery_partition_names
  end

  def get_recovery_key_comnibations
    Get.retry_on_http_connection_problem do
      @@recovery_key_combinations = YAML.load(URI.open("https://gitlab.com/free-droid/free-droid/raw/master/lookups/recovery_key_combinations.yml").read)
    end unless @@recovery_key_combinations
  end

  def get_aliases
    Get.retry_on_http_connection_problem do
      @@aliases = YAML.load(URI.open("https://gitlab.com/free-droid/free-droid/raw/master/lookups/aliases.yml").read)
    end unless @@aliases
  end

  # Attempts to find the name of the device if no parameter is provided
  # Return codename of a device given its model name
  def codename(name = "", options = {report_unknown: true})
    name = ADB.name if name.empty? unless $cli
    shortened_name = nil
    shortened_name = name[4..-1] if name.downcase.start_with?("htc_")
    shortened_name = name[5..-1] if name.downcase.start_with?("omni_") || name.downcase.start_with?("omni-")
    shortened_name = name[5..-1] if name.downcase.start_with?("sony ") || name.downcase.start_with?("sony-")
    shortened_name = name[7..-1] if name.downcase.start_with?("galaxy ") || name.downcase.start_with?("galaxy-")
    shortened_name = name[8..-1] if name.downcase.start_with?("samsung ") || name.downcase.start_with?("samsung-")
    shortened_name = name[8..-1] if name.downcase.start_with?("oneplus ") || name.downcase.start_with?("oneplus-")
    shortened_name = name[8..-1] if name.downcase.start_with?("aosp on ")
    get_codenames
    @@codenames.transform_keys!(&:downcase)
    if @@codenames[name.downcase]
      @@codenames[name.downcase]
    elsif shortened_name && @@codenames[shortened_name.downcase]
      @@codenames[shortened_name.downcase]
    elsif shortened_name && @@codenames.values.include?(shortened_name.downcase)
      shortened_name.downcase
    elsif @@codenames.values.include?(name.downcase)
      name.downcase
    elsif shortened_name && codename_from_model_csv(shortened_name)
      result = codename_from_model_csv(shortened_name)
      result = result[4..-1] if result.downcase.start_with?("htc_")
      result
    elsif codename_from_model_csv(name)
      result = codename_from_model_csv(name)
      result = result[4..-1] if result.downcase.start_with?("htc_")
      result
    else
      $reporter.report category: "unknown", action: name unless $cli || !options[:report_unknown]
      "unknown"
    end
  end

  @@codename_suffixes = ["_n", "_f", "_t", "_ds", "_nt", "_u2", "_ud2", "_uds", "_cdma", "_umts", "_udstv", "_umtsds"]

  # use return_array = true to get all possible codenames unfiltered
  def codename_from_model_csv(name = "", return_array = false)
    return nil unless get_csv
    name = ADB.name if name.empty? unless $cli
    # check if the given name already is the codename
    if @@table.any?{|row| row["Device"] == name}
      return return_array ? [name] : name
    end
    result = []
    @@table.each do |row|
      result << row["Device"].downcase if row["Model"] && row["Model"].downcase == name.downcase
    end
    # remove trailing _ds, _cdma and similar from the codenames.
    result.map! do |e|
      case
      when @@codename_suffixes.select{|s| s.length == 2}.include?(e[-2..-1]) then e[0..-3]
      when @@codename_suffixes.select{|s| s.length == 3}.include?(e[-3..-1]) then e[0..-4]
      when @@codename_suffixes.select{|s| s.length == 4}.include?(e[-4..-1]) then e[0..-5]
      when @@codename_suffixes.select{|s| s.length == 5}.include?(e[-5..-1]) then e[0..-6]
      when @@codename_suffixes.select{|s| s.length == 6}.include?(e[-6..-1]) then e[0..-7]
      when @@codename_suffixes.select{|s| s.length == 7}.include?(e[-7..-1]) then e[0..-8]
      else e
      end
    end unless return_array
    result.uniq!
    # if multiple codenames result and they all start with one of them
    # then take the this shortest one of them. Ex. "gts28wifi" and "gts28wifichn"
    if result.length > 1 && result.any? {|codename| result.all? {|e| e.start_with?(codename)}}
      result = [result.min_by(&:length)]
    end unless return_array
    if result.length == 1
      return_array ? result : result[0]
    elsif result.length > 1 && !$cli
      # look for matches of the possible results in the getprop of the device
      props = ADB.getprop
      result.select! {|r| props.lines.any? {|l| l.include?(r)}} unless props.empty?
      # return correct result if only one matches with getprop
      result.length == 1 ? result[0] : (return_array ? result : nil)
    else
      return_array ? result : nil
    end
  end

  def name_from_codename_csv(codename = "")
    return nil unless get_csv
    codename = $device.codename if codename.empty? unless $cli
    return nil if codename.nil? || codename.empty?
    result = []
    @@table.each do |row|
      result << row["Marketing Name"] if row["Device"] && row["Device"].downcase == codename.downcase
    end
    # retry with codename suffixes if nothing found
    if result.empty?
      @@codename_suffixes.each do |s|
        @@table.each do |row|
          result << row["Marketing Name"] if row["Device"] && row["Device"].downcase == "#{codename.downcase}#{s}"
        end
      end
    end
    result.select
    result.select{|r| r}.uniq!{|name| name.downcase.tr('[( _)]','')}
    result.length == 1 ? result[0] : result
  end

  def unique_name_from_codename_csv(codename = "")
    r = name_from_codename_csv(codename)
    r.is_a?(String) ? r : nil
  end

  def models_from_codename(codename = "")
    return nil unless get_csv
    codename = $device.codename if codename.empty? unless $cli
    return nil if codename.empty?
    result = models_from_codename_csv(codename)
    get_codenames
    @@codenames.transform_keys!(&:downcase)
    result = result + @@codenames.select{|k,v| v == codename}.keys
    result.uniq{|name| name.downcase.tr('[( _)]','')}
  end

  def models_from_codename_csv(codename = "")
    return nil unless get_csv
    codename = $device.codename if codename.empty? unless $cli
    return nil if codename.empty?
    result = []
    @@table.each do |row|
      result << row["Model"] if row["Device"] && row["Device"].downcase == codename.downcase
    end
    # retry with codename suffixes if nothing found
    if result.empty?
      @@codename_suffixes.each do |s|
        @@table.each do |row|
          result << row["Model"] if row["Device"] && row["Device"].downcase == "#{codename.downcase}#{s}"
        end
      end
    end
    result.uniq!(&:downcase)
    result
  end

  def brand_from_codename_csv(codename = "")
    return nil unless get_csv
    codename = $device.codename if codename.empty? unless $cli
    return nil if codename.empty?
    result = []
    @@table.each do |row|
      result << row["Retail Branding"] if row["Device"] && row["Device"].downcase == codename.downcase
    end
    # retry with codename suffixes if nothing found
    if result.empty?
      @@codename_suffixes.each do |s|
        @@table.each do |row|
          result << row["Retail Branding"] if row["Device"] && row["Device"].downcase == "#{codename.downcase}#{s}"
        end
      end
    end
    result.uniq!(&:downcase)
    result.length == 1 ? result[0] : result
  end

  # checks the yaml lookup file first
  def unique_brand_from_codename_csv(codename = "")
    codename.downcase!
    get_brands
    @@brands.transform_keys!(&:downcase)
    return @@brands[codename] if @@brands[codename]
    r = brand_from_codename_csv(codename)
    r = brand_from_codename_csv(Find.alias(codename)) if r.empty?
    r.is_a?(String) ? r : nil
  end

  # Returns the model names of all codenames marked as supported on the server
  def names(codename = nil)
    codename = Find.codename if codename.nil?
    get_codenames
    @@codenames.transform_keys!(&:downcase)
    @@codenames.select{|k,v| v == codename}.keys
  end

  def supported_devices_names
    get_supported
    result = []
    @@supported.select {|k,v| v}.keys.each { |codename| result.push(*names(codename)) }
    result
  end

  # Attempts to find the name of the recovery partition to flash TWRP to
  def recovery_partition_name(codename = "")
    if [nil, false, "unknown", ""].include?(codename) && !$cli
      codename = $device.codename ? $device.codename : $device.scan[:codename]
    end
    if [nil, false, "unknown", ""].include?(codename)
      nil
    else
      get_recovery_partitions_names
      @@recovery_partition_names[codename.to_s]
    end
  end

  # Attempts to find out if the given device is supported
  def supported?(codename = "")
    if codename.empty? && !$cli
      $device.codename ? codename = $device.codename : $device.scan[:codename]
    end
    if codename == "unknown"
      nil
    else
      get_supported
      @@supported[codename.to_s]
    end
  end

  # Must use unix find command because partitions can be mounted differently
  # in TWRP than specified in the fstab or even on the TWRP device page (!)
  def recovery_dev_path
    if $device.brand.downcase == "sony"
      result = ADB.shell("find /dev/ -iname FOTAKernel")
    else
      result = ADB.shell("find /dev/ -iname recovery")
      if result.nil? || result.empty?
        symlinkdest = ADB.shell("readlink -f /dev/block/by-name/recovery")
        result = symlinkdest unless symlinkdest.strip == "/dev/block/by-name/recovery"
      end
    end
    if result.lines.length >= 1
      result.lines[0].strip
    else
      nil
    end
  end unless $cli

  # gives the path to data partition containing "mmcblk" if possible (most direct one)
  def data_dev_path(candidates = data_dev_path_candidates)
    # returns nil if no candidates found
    candidates.select{|c| c.include?("mmcblk")}[0] || candidates[0]
  end unless $cli

  # find paths under which the partition mounted as /data is accessible
  def data_dev_path_candidates
    if TWRP.connected?
      candidates = []
      # first possibility
      fstab_data_line = ADB.shell("cat /etc/fstab").lines.select{|l| l.include?("/data")}[0]
      candidates << fstab_data_line.split(" ")[0] if fstab_data_line
      # second possibility
      fstab_data_line = ADB.shell("cat /etc/recovery.fstab").lines.select{|l| l.include?("/data")}[0]
      candidates << fstab_data_line.split(" ")[2] if fstab_data_line
      # third possibility
      twrp_data_line = TWRP.get_and_read_log.select{|l| l.start_with?("/data | /dev")}[0]
      data_path_from_twrp_log = twrp_data_line.split(" ").select{|e| e.include?("/dev/")}[0] rescue nil
      candidates << data_path_from_twrp_log if data_path_from_twrp_log
      candidates.uniq
    end
  end unless $cli

  def data_fs
    # ADB.shell("cat /etc/fstab").lines.select{|l| l.include?("/data")}[0].split(" ")[2] if TWRP.connected?
    if TWRP.connected?
      fstab_data_line = ADB.shell("cat /etc/fstab").lines.select{|l| l.include?("/data")}[0]
      return fstab_data_line.split(" ")[2] if fstab_data_line
      # second try
      fstab_data_line = ADB.shell("cat /etc/recovery.fstab").lines.select{|l| l.include?("/data")}[0]
      return fstab_data_line.split(" ")[1] if fstab_data_line
      # if unsuccessful return nil
      nil
    end
  end unless $cli

  def recovery_key_combination(codename = "")
    if codename.empty? && !$cli
      $device.codename ? codename = $device.codename : $device.scan[:codename]
    end
    result = if codename == "unknown"
      nil
    else
      get_recovery_key_comnibations
      @@recovery_key_combinations[codename.to_s]
    end
    if result.nil? && !$cli
      # case $device.brand
      # when "samsung" then "Now press and hold the VOLUME-DOWN + HOME + POWER buttons on your phone until the screen goes black and immediately switch from VOLUME-DOWN to VOL-UP."
      # when "sony" then "Now press and hold the VOLUME-UP + POWER buttons on your phone until you see the Sony Logo."
      # else ""
      # end
      generic = @@recovery_key_combinations[$device.brand.to_s.downcase]
      generic ? generic : ""
    else
      result
    end
  end

  def alias(codename, what = nil)
    get_aliases
    lookedup = @@aliases[codename.to_s]
    codenamexx = codename.end_with?("xx") ? codename.chop.chop : "#{codename}xx"
    case lookedup
    when String then lookedup
    when Hash then
      if what.to_s.upcase == "TWRP"
        lookedup["TWRP"]
      elsif lookedup[what.to_s]
        lookedup[what.to_s]
      # elsif lookedup[what.to_s].nil? && lookedup["rom"]
      elsif what.nil? && lookedup["rom"]
        lookedup["rom"]
      else
        codenamexx
      end
    else codenamexx
    end
  end

  def codename_ambiguous?(name = nil)
    name = ADB.name unless name unless $cli
    ["unknown", "", nil].include?(codename(name, report_unknown: false)) &&
      codename_from_model_csv(name, true).length > 1
  end

  def possible_candidates(name = nil)
    name = ADB.name unless name unless $cli
    result = []
    return result unless name
    result = Find.codename_from_model_csv(name, true).map do |c|
      [Find.models_from_codename(c)]
    end.flatten.uniq{|n| n.downcase.tr('[( _)]','')} - [name]
    $listbox_candidates_items = result unless $cli
    result
  end

  # list all devices which Free-Droid should work with but which are missing twrp or rom
  def devices_missing_twrp_or_rom
    get_codenames
    known = File.exist?("missing.yml") ? YAML.load(open("missing.yml").read) : {}
    @@codenames.values.uniq.each do |device|
      next if known[device] || (supported?(device) == false)
      available = Get.available_twrp_rom(device)
      what = "OK" if available.all?
      what = "TWRP" if available[1] && !available[0]
      what = "ROM" if available[0] && !available[1]
      what = "BOTH" if !available[0] && !available[1]
      puts "#{device}: #{what}"
      File.open("missing.yml", "a") do |file|
        file.puts "#{device}: #{what}"
      end
    end
  end

  @@last_generated_supported = nil

  # list all devices with official releases of TWRP and LineageOS
  # and that are from brands free-droid should be able to handle
  def officially_supported_devices
    puts "Scrape LineageOS..."
    lineageos = Get.scrape_lineageos || []
    puts "Scrape LineageOS for MicroG..."
    lineageos_microg = Get.scrape_lineageos_microg || []
    puts "Scrape Omnirom..."
    omnirom = Get.scrape_omnirom || []
    puts "Scrape Archive..."
    archive = Get.scrape_archive
    got_rom = (lineageos + lineageos_microg + omnirom).uniq
    puts "Scrape TWRP..."
    got_twrp = Get.scrape_twrp
    got_both = (got_rom & got_twrp) + archive - ["extras"]
    @@last_generated_supported = YAML.load(open("index.yml")) if File.exist?("index.yml")
    $supported = if !@@last_generated_supported || (@@last_generated_supported[:time].to_datetime < Time.now.to_datetime.prev_day(7))
      {time: Time.now, complete: false}
    else
      @@last_generated_supported
    end
    "Begin looping..."
    got_both.each do |codename|
      if Find.models_from_codename(codename).empty?
        puts "Skipping #{codename}: No known models."
      else
        puts "Looping through #{codename}..."
      end
      # for some codenames codename(models_from_codename(codename)[some]) != codename
      Find.models_from_codename(codename).uniq.each do |model|
        # [].flatten because sometimes an array and sometimes a string is returned
        model_name = [Find.name_from_codename_csv(codename)].flatten.uniq.join(" / ")
        model_name = [Find.name_from_codename_csv(Find.codename(model))].flatten.uniq.join(" / ") if model_name.empty?
        model_name = "" if model_name.empty?
        next if $supported[model_name] && $supported[model_name][model]
        brand = Find.unique_brand_from_codename_csv(Find.codename(model)) || ""
        availables = Get.available(Find.codename(model), urls: true)
        prepared = {roms: {versions: Get.available_versions(nil, availables), urls: availables}, 
          device_name: model_name,
          codename: Find.codename(model) == "unknown" ? "" : Find.codename(model),
          brand: brand,
          free_droid_support: if supported?(Find.codename(model)).nil?
            if @@supported_brands.include?(brand.downcase)
              if Get.available_twrp_rom(availables).all?
                "Unconfirmed"
              else
                "Missing TWRP or rom"
              end
            else
              "No"
            end
          else
            supported?(Find.codename(model)) ? "Confirmed" : "No"
          end
        }
        if $supported[model_name]
          $supported[model_name][model] = prepared
        else
          $supported[model_name] = {"#{model}" => prepared}
        end
        yield model, $supported[model_name][model] if block_given?
        # garbage collect to keep the heap from growing too big
        GC.start
      end
      File.open("index.yml", "w") { |f| f.write($supported.to_yaml) }
    end
    $supported[:complete] = true
    File.open("index.yml", "w") { |f| f.write($supported.to_yaml) }
  end

  def make_href(version, url)
    '<a href="' + url.to_s + '">' + version.to_s + '</a>'
  end

  def archive_make_href(versions, urls)
    m = versions.map{|k,v| make_href("#{k} #{v}", urls[k].to_s)}
    '<p>' + m.join('<br />') + '</p>'
  end

  def supported_devices_to_html_table(supported = nil)
    require "html/table"
    @@last_generated_supported = YAML.load(open("index.yml")) if File.exist?("index.yml")
    if !File.exist?("index.html") || (@@last_generated_supported && (@@last_generated_supported[:time].to_datetime < Time.now.to_datetime.prev_day(7)))
      FileUtils.cp "index_template.html", "index.html"
    end
    unless supported
      officially_supported_devices do |model, model_support|
        row = HTML::Table::Row.new { |r| 
          r.content = [model_support[:brand].to_s,
            model_support[:device_name].to_s,
            model.to_s,
            model_support[:codename].to_s,
            model_support[:free_droid_support],
            # ((model_support[:roms][:TWRP] || model_support[:roms][:Archive][:TWRP]) rescue "").to_s,
            Find.make_href(model_support[:roms][:versions][:TWRP], model_support[:roms][:urls][:TWRP]),
            Find.make_href(model_support[:roms][:versions][:LineageOS], model_support[:roms][:urls][:LineageOS]),
            Find.make_href(model_support[:roms][:versions][:ResurrectionRemix], model_support[:roms][:urls][:ResurrectionRemix]),
            Find.make_href(model_support[:roms][:versions][:Omnirom], model_support[:roms][:urls][:Omnirom]),
            # Find.make_href(model_support[:roms][:versions][:AospExtended], model_support[:roms][:urls][:AospExtended]),
            Find.make_href(model_support[:roms][:versions][:CarbonROM], model_support[:roms][:urls][:CarbonROM]),
            if model_support[:roms][:versions][:Archive]
              Find.archive_make_href(model_support[:roms][:versions][:Archive].reject{|k| [:override_TWRP].include?(k)}, model_support[:roms][:urls][:Archive].reject{|k| [:override_TWRP].include?(k)})
            else ""
            end]
          r.class_ = "item"
        }
        # Do not add to html table if the codename is unknown and there is nothing available
        if !model_support[:codename].empty? || model_support[:roms][:versions].reject{|k,v| [:Heimdall, :Platform_Tools, :NanoDroid, :Magisk].include?(k)}.values.any?
          File.open("index.html", 'a') { |file| file.puts(row.html) }
        end
      end
      File.open("index.html", 'a') { |file| file.puts('</tbody></table>') }
    else
      raise "Not yet implemented. Let this method loop through officially_supported_devices."
    end
  end

  def bin_dir
    if File.exist? "bin/Free-Droid.jar"
      "bin"
    elsif File.exist? "Free-Droid.jar"
      "."
    end
  end

end
end
