#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module to handle bootloader unlocking

require_relative "adb"
require_relative "fastboot"
require_relative "heimdall"


module Bootloader

  extend Log  # make log() method available as shortcut for Log.add()

  class << self

  def unlock(brand, codename)
    if self.respond_to?("unlock_#{brand.downcase}.to_sym")
      send("unlock_#{brand.downcase}.to_sym")
    else
      # log "No unlock method for #{brand}"
      # send brand to server
      false
    end
  end

  def unlock_samsung
    if Heimdall.connected?
      Heimdall.flash_recovery
    else
      log "No connection to your device."
      false
    end
  end

  end # class << self

end
