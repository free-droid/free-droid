#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module for determining running OS

module OS
  class << self
    def windows?
      if jruby?
        jruby_tell_which_os == "windows"
      else
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
      end
    end

    def mac?
      if jruby?
        jruby_tell_which_os == "mac"
      else
        (/darwin/ =~ RUBY_PLATFORM) != nil
      end
    end

    def unix?
      if jruby?
        jruby_tell_which_os == "unix"
      else
        !windows?
      end
    end

    def linux?
      if jruby?
        jruby_tell_which_os == "linux"
      else
        unix? and not mac?
      end
    end

    def jruby?
      RUBY_ENGINE == 'jruby'
    end

    def jruby_tell_which_os
      host_os = RbConfig::CONFIG['host_os']
      case host_os
      when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
        "windows"
      when /darwin|mac os/
        "mac"
      when /linux/
        "linux"
      when /solaris|bsd/
        "unix"
      else
        raise Error::WebDriverError, "unknown os: #{host_os.inspect}"
      end
    end

    def cpu
      host = RbConfig::CONFIG['host_cpu']
      target = RbConfig::CONFIG['target_cpu']
      if host == target
        host
      else
        "host: #{host}, target: #{target}"
      end
    end

    def which
      if jruby?
        jruby_tell_which_os
      else
        windows? ? "windows" : (mac? ? "mac" : "linux") rescue "unknown"
      end
    end

    def open_browser(link)
      if windows?
        system "start #{link.gsub(' ','+')}"
      elsif mac?
        system "open '#{link}'"
      elsif linux?
        system "xdg-open '#{link}'"
      end
    end

    # returns an array with width and height as integer
    def screen_res
      send("#{which}_screen_res".downcase.to_sym) rescue return 0, 0
    end

    def windows_screen_res
      res_cmd =  %x[wmic desktopmonitor get screenheight, screenwidth]
      res = res_cmd.split
      return res[3].to_i, res[2].to_i
    end

    def linux_screen_res
      `xrandr`.scan(/current (\d+) x (\d+)/).flatten.map(&:to_i)
    end

    def mac_screen_res
      `system_profiler SPDisplaysDataType | grep Resolution`.scan(/\d+/).flatten.map(&:to_i)
    end

  end # class << self
end
