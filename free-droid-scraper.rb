#!/bin/ruby

# Free-Droid scraper for available roms

# prevent loading the whole thing in required files
$cli = true

`wget https://gitlab.com/amo13/free-droid/raw/master/res/find.rb` unless File.exist?("find.rb")
`wget https://gitlab.com/amo13/free-droid/raw/master/res/get.rb` unless File.exist?("get.rb")
`wget https://gitlab.com/amo13/free-droid/raw/master/res/os.rb` unless File.exist?("os.rb")

require "csv"
require "yaml"
require "html/table"
require_relative "get"	# also requires find.rb and os.rb

def Get.log(input); end	# prevent errors in the Get backend

# require "pry"
Find.supported_devices_to_html_table

# binding.pry
# puts
